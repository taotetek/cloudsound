use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
use std::{thread, time};
use structopt::StructOpt;
use open_sound_module::{OscClient, CvAddress, CvMessage, TriggerAddress, TriggerMessage, OscMessage};

#[derive(Debug, StructOpt)]
#[structopt(
    name = "cloudsound",
    about = "Turn DO events into music"
)]
struct Config {
    #[structopt(short = "o", long = "osc_address")]
    osc_address: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let conf = Config::from_args();

    // millis per beat
    let beat_millis = time::Duration::from_millis(500);

    // millis to leave trigger set to 1 on beat
    let delay_millis = time::Duration::from_millis(10);
    
    // create osc client
    let osc_client = OscClient::new(conf.osc_address)?;

    // tx and rx channels - for now just using i32 to send a trigger
    // but will move to OscMessages
    let (tx, rx): (Sender<i32>, Receiver<i32>) = mpsc::channel();
    
    let clock_tx = tx.clone();
    let clock_thread = thread::spawn(move || {
        loop {
            clock_tx.send(0).unwrap();
            println!("clock_thread sent beat");
            thread::sleep(beat_millis);
        }
    });

    let clock_on = TriggerMessage::new(TriggerAddress::A, 1);
    let clock_off = TriggerMessage::new(TriggerAddress::A, 0);

    loop {
        let _b = rx.recv();
        println!("main thread received beat");
        osc_client.send_osc_message(OscMessage::TriggerMessage(clock_on))?;
        thread::sleep(delay_millis);
        osc_client.send_osc_message(OscMessage::TriggerMessage(clock_off))?;
    }

    Ok(())
}
