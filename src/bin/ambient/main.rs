use structopt::StructOpt;
use serde_derive::{Deserialize, Serialize};
use open_sound_module::{OscClient, CvAddress, CvMessage, TriggerAddress, TriggerMessage, OscMessage};

#[derive(Debug, StructOpt)]
#[structopt(
    name = "cloudsound",
    about = "Turn DO events into music"
)]
struct Config {
    #[structopt(short = "z", long = "zmq_address")]
    zmq_address: String,
    #[structopt(short = "t", long = "topic")]
    zmq_topic: String,
    #[structopt(short = "o", long = "osc_address")]
    osc_address: String,
}

#[derive(Serialize, Deserialize)]
struct EventMsg {
    event_type: String,
    action_status: String,
    runtime: String,
}

static PENTATONIC2: &'static [f32] = &[-0.333, -0.300, -0.266, -0.220, -0.185];
static PENTATONIC3: &'static [f32] = &[-0.137, -0.105, -0.071, -0.024, -0.009];

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let conf = Config::from_args();

    let osc_client = OscClient::new(conf.osc_address)?;    
    let ctx = zmq::Context::new();
    let subscriber = ctx.socket(zmq::SUB)?;

    subscriber.set_subscribe(&conf.zmq_topic.as_bytes())?;
    subscriber.connect(&conf.zmq_address)?;

    let mut msg = zmq::Message::new();

    loop {
        subscriber.recv(&mut msg, 0)?;
        let more = msg.get_more();
        if !more {
            panic!("received single part message, should not happen");
        }

        subscriber.recv(&mut msg, 0)?;
        let more = msg.get_more();
        if more {
            panic!("received > 2 part message, should not happen");
        }

        let data = msg.as_str().unwrap();
        let e: EventMsg = serde_json::from_str(data)?;
        println!("event_type: {} action_status: {} runtime: {}", e.event_type, e.action_status, e.runtime);

        let v = match e.event_type.as_ref() {
            "create" => PENTATONIC3[0],
            "destroy" => PENTATONIC3[1],
            "detach_drive" => PENTATONIC3[2],
            "droplet_metadata_create" => PENTATONIC3[3],
            "droplet_metadata_delete" => PENTATONIC3[4],
            "backup" => PENTATONIC2[0],
            "power_on" => PENTATONIC2[1],
            "power_off" => PENTATONIC2[2],
            _ => PENTATONIC2[4],
        };

        if e.action_status == "done" {
            let note = CvMessage::new(CvAddress::A, v);
            let on = TriggerMessage::new(TriggerAddress::A, 1);
            let off = TriggerMessage::new(TriggerAddress::A, 0);
            osc_client.send_osc_message(OscMessage::CvMessage(note))?;
            osc_client.send_osc_message(OscMessage::TriggerMessage(on))?;
            osc_client.send_osc_message(OscMessage::TriggerMessage(off))?;
        } else {
            let on = TriggerMessage::new(TriggerAddress::B, 1);
            let off = TriggerMessage::new(TriggerAddress::B, 0);
            osc_client.send_osc_message(OscMessage::TriggerMessage(on))?;
            osc_client.send_osc_message(OscMessage::TriggerMessage(off))?;
            osc_client.send_osc_message(OscMessage::TriggerMessage(on))?;
            osc_client.send_osc_message(OscMessage::TriggerMessage(off))?;
        }
    }

    Ok(())
}
